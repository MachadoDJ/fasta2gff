# FASTA2GFF

## Description

This is a Python v3+ script that depends on Biopython. The script reads a multi-FASTA file (i.e., a FASTA file with several entries) and returns a GFF file. The GFF file assumes that every single entry on the FASTA file corresponds to a gene.

## Usage

Execute `python3 fast2gff.py` to see the following usage information:

```
usage: fasta2gff.py [-h] [-a ATTRIBUTE] [-c SCORE] [-f FASTA] [-g] [-m FRAME] [-r STRAND] [-s SOURCE] [-t FEATURE]

optional arguments:
  -h, --help            show this help message and exit
  -a ATTRIBUTE, --attribute ATTRIBUTE
                        A semicolon-separated list of tag-value pairs (default = '.')
  -c SCORE, --score SCORE
                        A floating point value (default = '.')
  -f FASTA, --fasta FASTA
                        FASTA file
  -g, --gzip            Increases output verbosity
  -m FRAME, --frame FRAME
                        One of '0', '1' or '2' (default = '0')
  -r STRAND, --strand STRAND
                        defined as + (forward) or - (reverse) (default = '+')
  -s SOURCE, --source SOURCE
                        name of the program that generated this feature (default = '.')
  -t FEATURE, --feature FEATURE
                        feature type name, e.g. Gene, Variation, Similarity (default = 'transcript'
```

## Example

This example is using cDNA data from the improved genome assembly and annotation (v6.1) for the doubled monoploid potato DM 1-3 516 R44 (available from http://solanaceae.plantbiology.msu.edu/dm_v6_1_download.shtml, accessed on Apr. 12, 2021).

Execute as:

```
python3 fasta2gff.py -g -f DM_1-3_516_R44_potato.v6.1.hc_gene_models.cdna.fa.gz > DM_1-3_516_R44_potato.v6.1.hc_gene_models.cdna.gff
```

The script will print a GFF table to the standard output, which is being redirected to a file named `DM_1-3_516_R44_potato.v6.1.hc_gene_models.cdna.gff`.

## References

- "GFF/GTF File Format - Definition and supported options." available from https://useast.ensembl.org/info/website/upload/gff.html. Accessed on April 12, 2021.
