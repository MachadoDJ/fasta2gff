#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Name: fasta2gff.py
# Description: The script reads a multi-FASTA file (i.e., a FASTA file with several entries) and returns a GFF3 file. The GFF3 file assumes that every single entry on the FASTA file correspond to a gene.

##
# Import libraries
##

import argparse, re, sys, gzip
from Bio import SeqIO
from Bio import SeqIO

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-a", "--attribute", help = "A semicolon-separated list of tag-value pairs (default = '.')", type = str, default = ".", required = False)
parser.add_argument("-c", "--score", help = "A floating point value (default = '.')", type = str, default = ".", required = False)
parser.add_argument("-f", "--fasta", help = "FASTA file", type = str)
parser.add_argument("-g", "--gzip", help = "Increases output verbosity", action = "store_true")
parser.add_argument("-m", "--frame", help = "One of '0', '1' or '2' (default = '0')", type = str, default = "0", required = False)
parser.add_argument("-r", "--strand", help = "defined as + (forward) or - (reverse) (default = '+')", type = str, default = "+", required = False)
parser.add_argument("-s", "--source", help = "name of the program that generated this feature (default = '.')", type = str, default = ".", required = False)
parser.add_argument("-t", "--feature", help = "feature type name, e.g. Gene, Variation, Similarity (default = 'transcript')", type = str, default = "transcript", required = False)
args = parser.parse_args()

##
# Define functions
##

def printo(var):
	return sys.stdout.write(str(var).strip() + "\n")

def printe(var):
	return sys.stderr.write(str(var).strip() + "\n")


def main():

	source = args.source
	feature = args.feature
	start = "1"
	score = args.score
	strand = args.strand
	frame = args.frame
	attribute = args.attribute
	
	printo("# seqname\tsource\tfeature\tstart\tend\tscore\tstrand\tframe\tattribute\n")
	
	if args.gzip:
		handle = gzip.open(args.fasta, "rt")
	else:
		handle = open(args.fasta, "r")
	for record in SeqIO.parse(handle, "fasta"):
		seqname = str(record.id)
		end = len(str(record.seq))
		printo("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(seqname, source, feature, start, end, score, strand, frame, attribute))
	return

##
# Execute functions
##

main() # This is the main fuction
exit() # Quit this script
